var annotated_dup =
[
    [ "config", "classconfig.html", "classconfig" ],
    [ "fftw", "classfftw.html", "classfftw" ],
    [ "fftw2D", "classfftw2D.html", "classfftw2D" ],
    [ "field", "classfield.html", "classfield" ],
    [ "gfield", "classgfield.html", "classgfield" ],
    [ "gmatrix", "classgmatrix.html", "classgmatrix" ],
    [ "lfield", "classlfield.html", "classlfield" ],
    [ "lmatrix", "classlmatrix.html", "classlmatrix" ],
    [ "matrix", "classmatrix.html", "classmatrix" ],
    [ "momenta", "classmomenta.html", "classmomenta" ],
    [ "mpi_class", "classmpi__class.html", "classmpi__class" ],
    [ "MV_class", "classMV__class.html", "classMV__class" ],
    [ "positions", "classpositions.html", "classpositions" ],
    [ "rand_class", "classrand__class.html", "classrand__class" ],
    [ "su3_matrix", "classsu3__matrix.html", "classsu3__matrix" ]
];