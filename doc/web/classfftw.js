var classfftw =
[
    [ "fftw", "classfftw.html#aeb782ce88f10b881476beb8640689bd3", null ],
    [ "~fftw", "classfftw.html#aa42c210855d54c4101fd3846cb080eef", null ],
    [ "alloc_local", "classfftw.html#a962d0de556305b0a1d0f2d4a3fdfead6", null ],
    [ "alloc_local_single", "classfftw.html#a4049e44cb1f01cfff9237b7dd1d51e1e", null ],
    [ "data_local", "classfftw.html#a55b4041084927873d34dc9ca72313a36", null ],
    [ "data_local_single", "classfftw.html#aa765bc6f12cd3cf7928f3ea4fcacbaed", null ],
    [ "local_n0", "classfftw.html#a3aa9384851780d211187ccf484c006e8", null ],
    [ "local_n0_single", "classfftw.html#a014722a96213ce916994b331e423533f", null ],
    [ "local_n0_start", "classfftw.html#a688088e8be43346c900af01fccae96e7", null ],
    [ "local_n0_start_single", "classfftw.html#a64454e2142d387a5c976447df7b68e64", null ],
    [ "N0", "classfftw.html#a57ede8b68730ea2702a290aa67d25f04", null ],
    [ "N1", "classfftw.html#a5bf35c748851f2ee90ce0c5d6f816321", null ],
    [ "Nxl", "classfftw.html#a6b529701d14b610eb3381f2ca7f7f040", null ],
    [ "Nyl", "classfftw.html#a56e856ebc2a5c2d9474d6ece588714ca", null ],
    [ "planK2X", "classfftw.html#af8680b0f1c6d486d7a0a014862adfd2b", null ],
    [ "planK2X_single", "classfftw.html#ac5d9fe1d3ef271fc59cd4f99e9345641", null ],
    [ "planX2K", "classfftw.html#ae3f4728c8596223c55cf26a756a87f62", null ],
    [ "planX2K_single", "classfftw.html#a54548f2fd0f7bf959c94beacc05e40b8", null ],
    [ "volume", "classfftw.html#a5466af4bbaad898b1ee005f98c985508", null ]
];