var config_8h =
[
    [ "config", "classconfig.html", "classconfig" ],
    [ "Nx", "config_8h.html#af093d941bc37a7a3f38e1db6593d9633", null ],
    [ "Ny", "config_8h.html#a104a7795534fc75b6dbc144e96ff0bae", null ],
    [ "Coupling", "config_8h.html#a967b0559766c873f7c65c83cb9428a09", [
      [ "SQRT_COUPLING_CONSTANT", "config_8h.html#a967b0559766c873f7c65c83cb9428a09ab037784ad5f2392f8ad55ebb9538d8c7", null ],
      [ "NOISE_COUPLING_CONSTANT", "config_8h.html#a967b0559766c873f7c65c83cb9428a09a86eaed9fc51d851e6abcc2f49dc5c2cc", null ],
      [ "HATTA_COUPLING_CONSTANT", "config_8h.html#a967b0559766c873f7c65c83cb9428a09a1c90fcdf19ac8f3a13db0f5db32f2ba8", null ],
      [ "NO_COUPLING_CONSTANT", "config_8h.html#a967b0559766c873f7c65c83cb9428a09a9cda83473e0513925e4ee6b4e687f89f", null ]
    ] ],
    [ "Evolution", "config_8h.html#a7759da59005c21f899b795ffa18e3487", [
      [ "POSITION_EVOLUTION", "config_8h.html#a7759da59005c21f899b795ffa18e3487afc7b07b6c499990eee401fb0e03586d2", null ],
      [ "MOMENTUM_EVOLUTION", "config_8h.html#a7759da59005c21f899b795ffa18e3487a9dc71fbff1a63901882c48d77cf17ea0", null ],
      [ "NO_EVOLUTION", "config_8h.html#a7759da59005c21f899b795ffa18e3487aa8167a8300e4a50f056a07244c3f0970", null ]
    ] ],
    [ "Kernel", "config_8h.html#a8d71fe894b897c26adb0313a574b9839", [
      [ "LINEAR_KERNEL", "config_8h.html#a8d71fe894b897c26adb0313a574b9839a44c19885e8509cdfeb89db38bb74e4bb", null ],
      [ "SIN_KERNEL", "config_8h.html#a8d71fe894b897c26adb0313a574b9839abc78c3c9badd15c27e310f790e7dde8f", null ]
    ] ]
];