var hierarchy =
[
    [ "config", "classconfig.html", null ],
    [ "fftw", "classfftw.html", [
      [ "fftw2D", "classfftw2D.html", null ]
    ] ],
    [ "field< T, t >", "classfield.html", [
      [ "gfield< T, t >", "classgfield.html", null ],
      [ "lfield< T, t >", "classlfield.html", null ]
    ] ],
    [ "matrix< T >", "classmatrix.html", [
      [ "gmatrix< T >", "classgmatrix.html", null ],
      [ "lmatrix< T >", "classlmatrix.html", null ]
    ] ],
    [ "momenta", "classmomenta.html", null ],
    [ "mpi_class", "classmpi__class.html", null ],
    [ "MV_class", "classMV__class.html", null ],
    [ "positions", "classpositions.html", null ],
    [ "rand_class", "classrand__class.html", null ],
    [ "su3_matrix< T >", "classsu3__matrix.html", null ]
];