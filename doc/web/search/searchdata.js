var indexSectionsWithContent =
{
  0: "abcdefghijklmnoprstuvxyz~",
  1: "cfglmprs",
  2: "cfimprstz",
  3: "abdefghilmnoprstuxz~",
  4: "acdefgklmnprstuvxy",
  5: "cek",
  6: "hlmnps",
  7: "fg",
  8: "mns",
  9: "ij"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "related",
  8: "defines",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Friends",
  8: "Macros",
  9: "Pages"
};

