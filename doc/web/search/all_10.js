var searchData=
[
  ['rand_5fclass',['rand_class',['../classrand__class.html',1,'rand_class'],['../classrand__class.html#a8e8306d5aa18a6729268757934c56d82',1,'rand_class::rand_class()']]],
  ['rand_5fclass_2eh',['rand_class.h',['../rand__class_8h.html',1,'']]],
  ['rank',['rank',['../classmpi__class.html#abe455781ac0b51ca2b98e1e44b7d88c9',1,'mpi_class']]],
  ['read_5fconfig_5ffrom_5ffile',['read_config_from_file',['../classconfig.html#a3db5cd0b2853fec8841e85403af5548f',1,'config']]],
  ['readme_2emd',['README.md',['../README_8md.html',1,'']]],
  ['reduce',['reduce',['../classgfield.html#a743dc1eeb1b0756557a3173895df1c8d',1,'gfield']]],
  ['reduce_5fhatta',['reduce_hatta',['../classgfield.html#a16112e9685d6442818f2a308811620f6',1,'gfield']]],
  ['reduceandset',['reduceAndSet',['../classlfield.html#ad624c0368bcb52153abf6568d1bce971',1,'lfield']]],
  ['rgenerator',['rgenerator',['../classrand__class.html#a3522475460a09e6f44acc8c32e3d36e1',1,'rand_class']]],
  ['row_5fcomm',['row_comm',['../classmpi__class.html#a9380f9b5df9f135223ad1491a19f570e',1,'mpi_class']]]
];
