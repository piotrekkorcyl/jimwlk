var searchData=
[
  ['p_5fpos_5fx',['p_pos_x',['../classmpi__class.html#a89b4a858436a53e9023fa48003eeb0be',1,'mpi_class']]],
  ['p_5fpos_5fy',['p_pos_y',['../classmpi__class.html#ad890412e341bc957c112946345c2a369',1,'mpi_class']]],
  ['plank2x',['planK2X',['../classfftw.html#af8680b0f1c6d486d7a0a014862adfd2b',1,'fftw']]],
  ['plank2x_5fsingle',['planK2X_single',['../classfftw.html#ac5d9fe1d3ef271fc59cd4f99e9345641',1,'fftw']]],
  ['planx2k',['planX2K',['../classfftw.html#ae3f4728c8596223c55cf26a756a87f62',1,'fftw']]],
  ['planx2k_5fsingle',['planX2K_single',['../classfftw.html#a54548f2fd0f7bf959c94beacc05e40b8',1,'fftw']]],
  ['pos_5fx',['pos_x',['../classmomenta.html#ae7f2ef63b73037486694f9b3bbb27edb',1,'momenta::pos_x()'],['../classmpi__class.html#afac4c16b71138b83be09735b3ccceb9c',1,'mpi_class::pos_x()'],['../classpositions.html#ae250a39c32f4e37f1992d2d222236038',1,'positions::pos_x()']]],
  ['pos_5fy',['pos_y',['../classmomenta.html#adc0c9e39fb31444773d2843886ab2d66',1,'momenta::pos_y()'],['../classmpi__class.html#aba913ec9533501fd01b5ade9a9638f8c',1,'mpi_class::pos_y()'],['../classpositions.html#a302863858022454259f99905af44c0bc',1,'positions::pos_y()']]],
  ['proc_5fgrid',['proc_grid',['../classmpi__class.html#aded9ccd3992b336111874a26285089a6',1,'mpi_class']]],
  ['proc_5fx',['proc_x',['../classconfig.html#a4d9df6b6420dfb3c96215760674938ab',1,'config']]],
  ['proc_5fy',['proc_y',['../classconfig.html#a5a0fb0959e71353e8ed65eddb30adb74',1,'config']]]
];
