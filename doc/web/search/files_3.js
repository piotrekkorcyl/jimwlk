var searchData=
[
  ['main_5fexplicit_2ecpp',['main_explicit.cpp',['../main__explicit_8cpp.html',1,'']]],
  ['main_5foptimized_2ecpp',['main_optimized.cpp',['../main__optimized_8cpp.html',1,'']]],
  ['matrix_2eh',['matrix.h',['../matrix_8h.html',1,'']]],
  ['momenta_2eh',['momenta.h',['../momenta_8h.html',1,'']]],
  ['mpi_5fclass_2eh',['mpi_class.h',['../mpi__class_8h.html',1,'']]],
  ['mpi_5fexchange_5fboundaries_2ecpp',['mpi_exchange_boundaries.cpp',['../mpi__exchange__boundaries_8cpp.html',1,'']]],
  ['mpi_5fexchange_5fgrid_2ecpp',['mpi_exchange_grid.cpp',['../mpi__exchange__grid_8cpp.html',1,'']]],
  ['mpi_5fexchange_5fgroups_2ecpp',['mpi_exchange_groups.cpp',['../mpi__exchange__groups_8cpp.html',1,'']]],
  ['mpi_5fexchange_5fgroups_2eh',['mpi_exchange_groups.h',['../mpi__exchange__groups_8h.html',1,'']]],
  ['mpi_5ffftw_5fclass_2eh',['mpi_fftw_class.h',['../mpi__fftw__class_8h.html',1,'']]],
  ['mpi_5finit_2ecpp',['mpi_init.cpp',['../mpi__init_8cpp.html',1,'']]],
  ['mv_5fclass_2eh',['MV_class.h',['../MV__class_8h.html',1,'']]]
];
