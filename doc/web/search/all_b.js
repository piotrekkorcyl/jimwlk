var searchData=
[
  ['langevin_5fsteps',['langevin_steps',['../classconfig.html#a8f2963b883e1d3028b0ef93fdc213d91',1,'config']]],
  ['lfield',['lfield',['../classlfield.html',1,'lfield&lt; T, t &gt;'],['../classlfield.html#a64f15979565df6adfb1a90bb45a4af7f',1,'lfield::lfield(int NNx, int NNy)'],['../classlfield.html#a20b8f55a07ab66eda4152d1e4dc78b36',1,'lfield::lfield(const lfield&lt; T, t &gt; &amp;in)']]],
  ['linear_5fkernel',['LINEAR_KERNEL',['../config_8h.html#a8d71fe894b897c26adb0313a574b9839a44c19885e8509cdfeb89db38bb74e4bb',1,'config.h']]],
  ['lmatrix',['lmatrix',['../classlmatrix.html',1,'lmatrix&lt; T &gt;'],['../classlmatrix.html#af299002f3689796c8a6f037dee0e834b',1,'lmatrix::lmatrix()']]],
  ['loc_5fpos',['loc_pos',['../classlfield.html#aaef63739ec1d434f5f0459b45df11f14',1,'lfield']]],
  ['local_5fn0',['local_n0',['../classfftw.html#a3aa9384851780d211187ccf484c006e8',1,'fftw']]],
  ['local_5fn0_5fsingle',['local_n0_single',['../classfftw.html#a014722a96213ce916994b331e423533f',1,'fftw']]],
  ['local_5fn0_5fstart',['local_n0_start',['../classfftw.html#a688088e8be43346c900af01fccae96e7',1,'fftw']]],
  ['local_5fn0_5fstart_5fsingle',['local_n0_start_single',['../classfftw.html#a64454e2142d387a5c976447df7b68e64',1,'fftw']]]
];
