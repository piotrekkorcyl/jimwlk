var files =
[
    [ "tests", "dir_59425e443f801f1f2fd8bbe4959a3ccf.html", "dir_59425e443f801f1f2fd8bbe4959a3ccf" ],
    [ "zheevh3-C-1.1", "dir_c5f3485f3e2b5115505888d7d6814647.html", "dir_c5f3485f3e2b5115505888d7d6814647" ],
    [ "config.h", "config_8h.html", "config_8h" ],
    [ "field.h", "field_8h.html", "field_8h" ],
    [ "main_explicit.cpp", "main__explicit_8cpp.html", "main__explicit_8cpp" ],
    [ "main_optimized.cpp", "main__optimized_8cpp.html", "main__optimized_8cpp" ],
    [ "matrix.h", "matrix_8h.html", [
      [ "matrix", "classmatrix.html", "classmatrix" ],
      [ "lmatrix", "classlmatrix.html", "classlmatrix" ],
      [ "gmatrix", "classgmatrix.html", "classgmatrix" ],
      [ "lmatrix", "classlmatrix.html", "classlmatrix" ]
    ] ],
    [ "momenta.h", "momenta_8h.html", [
      [ "momenta", "classmomenta.html", "classmomenta" ]
    ] ],
    [ "mpi_class.h", "mpi__class_8h.html", [
      [ "mpi_class", "classmpi__class.html", "classmpi__class" ]
    ] ],
    [ "mpi_exchange_boundaries.cpp", "mpi__exchange__boundaries_8cpp.html", null ],
    [ "mpi_exchange_grid.cpp", "mpi__exchange__grid_8cpp.html", null ],
    [ "mpi_exchange_groups.cpp", "mpi__exchange__groups_8cpp.html", null ],
    [ "mpi_exchange_groups.h", "mpi__exchange__groups_8h.html", "mpi__exchange__groups_8h" ],
    [ "mpi_fftw_class.h", "mpi__fftw__class_8h.html", [
      [ "fftw", "classfftw.html", "classfftw" ],
      [ "fftw2D", "classfftw2D.html", "classfftw2D" ]
    ] ],
    [ "mpi_init.cpp", "mpi__init_8cpp.html", null ],
    [ "MV_class.h", "MV__class_8h.html", [
      [ "MV_class", "classMV__class.html", "classMV__class" ]
    ] ],
    [ "positions.h", "positions_8h.html", [
      [ "positions", "classpositions.html", "classpositions" ]
    ] ],
    [ "rand_class.h", "rand__class_8h.html", [
      [ "rand_class", "classrand__class.html", "classrand__class" ]
    ] ],
    [ "saturation.cpp", "saturation_8cpp.html", "saturation_8cpp" ],
    [ "su3_matrix.h", "su3__matrix_8h.html", "su3__matrix_8h" ]
];